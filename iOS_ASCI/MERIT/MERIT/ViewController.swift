//
//  ViewController.swift
//  MERIT
//
//  Created by Madhu on 27/03/21.
//

import UIKit
import WebKit



class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    
    let animatedImg = UIImageView(image: UIImage(named:"ic_launcher"))

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        do{
            if(try AppUpdatesChecker.hasUpdateAvailableOnAppstore())
            {
                self.webView.alpha = 0
                DispatchQueue.main.async
                {
                let alert = UIAlertController(title: "App Update", message: "A new update to this app is available.\nPlease update.", preferredStyle: .alert)
                    
                alert.addAction(UIAlertAction(title: "Ignore", style: .default, handler: { (action) in
                    self.setUpWebView()
                }))
                    
                alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action) in
                    self.setUpWebView()
                }))
                
                self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                setUpWebView()
            }
            
        }catch{
            setUpWebView()
        }
        
        
        
    }
    
    func setUpWebView()
    {
        let collageURL = URL(string: AppConstants.appURL)
        let urlRequest = URLRequest(url: collageURL!)
        webView.load(urlRequest)
        webView.allowsBackForwardNavigationGestures = true
        webView.navigationDelegate = self
        webView.alpha = 0
    }
    
}

extension ViewController : WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!)
    {
        print(#function)
        animatedImg.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(animatedImg)
        
        let constraints = [
            animatedImg.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            animatedImg.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            animatedImg.widthAnchor.constraint(equalToConstant: 128),
            animatedImg.heightAnchor.constraint(equalToConstant: 128)
        ]
        NSLayoutConstraint.activate(constraints)
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn) {
            self.animatedImg.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        } completion: { (_) in
            self.animatedImg.removeFromSuperview()
            self.webView.alpha = 1
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        print(#function)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
       // self.animatedImg.removeFromSuperview()
        
        
    }
    
}

