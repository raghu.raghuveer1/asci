//
//  AppUpdateChecker.swift
//  MERIT
//
//  Created by Madhu on 28/03/21.
//

import Foundation

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}


struct AppUpdatesChecker
{
    static func hasUpdateAvailableOnAppstore() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "\(AppConstants.iTunesURL+identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return version != currentVersion
        }
        return true
        throw VersionError.invalidResponse
    }
    
}
