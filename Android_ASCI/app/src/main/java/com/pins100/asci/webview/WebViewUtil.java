package com.pins100.asci.webview;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.ScaleAnimation;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.pins100.asci.R;
import com.pins100.asci.util.AppConstants;
import com.pins100.asci.util.AppUtils;
import com.pins100.asci.util.PermissionUtils;
import com.pins100.asci.views.MainActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Utility class common for all the functions related to Webview
 */

public class WebViewUtil extends RelativeLayout
{
    private Context context;
    public static final int PROGRESS_BAR_STYLE_NONE = 0;
    public static final int PROGRESS_BAR_STYLE_LINEAR = 1;
    public static final int PROGRESS_BAR_STYLE_CIRCULAR = 2;

    //attributes
    private int progressBarStyle = PROGRESS_BAR_STYLE_LINEAR;
    private int inflationDelay = 100;
    private boolean enableJavaScript = false;
    private boolean overrideTelLink = true;
    private boolean overrideEmailLink = true;
    private boolean overridePdfLink = true;
    private boolean showZoomControl = false;
    private boolean enableVerticalScrollBar = false;
    private boolean enableHorizontalScrollBar = false;
    private String url;
    private boolean isHide=false;
    private ImageView logoImg;
    private ScaleAnimation shrinkAnim;

    //For loading static data
    private String htmlData;
    private String mimeType;
    private String encoding;

    //View elements
    private ViewStub webViewStub;
    private WebView webView;
    private ProgressBar linearProgressBar;
    private ProgressBar circularProgressBar;
    private ProgressBar customProgressBar;

    private boolean webViewInflated = false;

    private WebViewSuiteCallback callback;
    private WebViewSetupInterference interference;
    private WebViewOpenPDFCallback openPDFCallback;

    private OnScrollChangedCallback mOnScrollChangedCallback;
    public Uri mCapturedImageURI=null;
    public ValueCallback<Uri[]> mUploadMessage;
    public String mCameraPhotoPath = null;
    public static final int INPUT_FILE_REQUEST_CODE = 111;
    public static final int INPUT_CAMERA_REQUEST_CODE = 112;
    public static final int INPUT_AUDIO_FILE_REQUEST_CODE = 113;
    public long size = 0;
    private WebViewInterface mWebInterface;
    private ArrayList<String> imagesList;
    private boolean dontLoadPage=false;



    public WebViewUtil(@NonNull Context context) {
        super(context);
        init(context);
    }

    public WebViewUtil(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WebViewSuite, 0, 0);
        try {
            progressBarStyle = a.getInt(R.styleable.WebViewSuite_webViewProgressBarStyle, PROGRESS_BAR_STYLE_LINEAR);
            inflationDelay = a.getInt(R.styleable.WebViewSuite_inflationDelay, 100);
            enableJavaScript = a.getBoolean(R.styleable.WebViewSuite_enableJavaScript, false);
            overrideTelLink = a.getBoolean(R.styleable.WebViewSuite_overrideTelLink, true);
            overrideEmailLink = a.getBoolean(R.styleable.WebViewSuite_overrideEmailLink, true);
            overridePdfLink = a.getBoolean(R.styleable.WebViewSuite_overridePdfLink, true);
            showZoomControl = a.getBoolean(R.styleable.WebViewSuite_showZoomControl, false);
            enableVerticalScrollBar = a.getBoolean(R.styleable.WebViewSuite_enableVerticalScrollBar, false);
            enableHorizontalScrollBar = a.getBoolean(R.styleable.WebViewSuite_enableHorizontalScrollBar, false);
            url = AppConstants.URL;
        } finally {
            a.recycle();
        }
        init(context);
    }

    public WebViewUtil(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    /**
     * Init function to
     * @param context
     */
    private void init(Context context) {
        this.context = context;
        View rootView = inflate(context, R.layout.web_view_suite, this);

        webViewStub = rootView.findViewById(R.id.webview_stub);
        linearProgressBar = rootView.findViewById(R.id.linear_progressbar);
        circularProgressBar = rootView.findViewById(R.id.circular_progressbar);

        switch (progressBarStyle) {
            case PROGRESS_BAR_STYLE_CIRCULAR:
                linearProgressBar.setVisibility(GONE);
                circularProgressBar.setVisibility(VISIBLE);
                break;
            case PROGRESS_BAR_STYLE_NONE:
                linearProgressBar.setVisibility(GONE);
                circularProgressBar.setVisibility(GONE);
                break;
            case PROGRESS_BAR_STYLE_LINEAR:
            default:
                circularProgressBar.setVisibility(GONE);
                linearProgressBar.setVisibility(VISIBLE);
        }

        Handler webViewInflationHandler = new Handler();
        webViewInflationHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView = (WebView) webViewStub.inflate();
                webViewInflated = true;
                postWebViewInflated();
            }
        }, inflationDelay);

    }

    private void postWebViewInflated () {
        if (!webViewInflated || webView == null) return;
        setupWebView();
        if (url != null && !url.isEmpty()) {
            webView.loadUrl(AppUtils.getInstance().getToken(0,context));
        } else if (htmlData != null && !htmlData.isEmpty()) {
            webView.loadData(htmlData, mimeType, encoding);
        }
    }

    /**
     * Submit your URL programmatically.
     * This will of course override the URL you set in XML (if any).
     * You can do this in onCreate() of your activity, because even if webView is null,
     * loading will be triggered again after webView is inflated.
     */
    public void startLoading (String url) {
        this.url = url;
        if (!webViewInflated || webView == null) return;
        webView.loadUrl(url);
    }

    public void startLoadData (String data, String mimeType, String encoding) {
        this.htmlData = data;
        this.mimeType = mimeType;
        this.encoding = encoding;
        if (!webViewInflated || webView == null) return;
        webView.loadData(htmlData, mimeType, encoding);
    }



    /**
     * A convenient method for you to override your onBackPressed.
     * return false if there is no more page to goBack / webView is not yet inflated.
     */
    public boolean goBackIfPossible () {
        if (webView != null && webView.canGoBack()) {
            webView.setVisibility(View.VISIBLE);
            isHide=true;
            webView.goBack();
            return true;
        } else {
            return false;
        }
    }

    /**
     * A convenient method for you to refresh.
     */
    public void refresh () {
        if (webView != null) webView.reload();
    }

    /**
     * If you don't like default progressbar, you can simply submit your own through this method.
     * It will automatically disappear and reappear according to page load.
     */
    public void setCustomProgressBar (ProgressBar progressBar) {
        this.customProgressBar = progressBar;
    }


    public void setLoadImage(ImageView logoImage,ScaleAnimation shrinkAnim) {
        this.logoImg = logoImage;
        this.shrinkAnim=shrinkAnim;
    }

    public void setWebInterface(WebViewInterface webInterface)
    {
        this.mWebInterface=webInterface;
    }



    public void toggleProgressbar (boolean isVisible) {
        int status = isVisible ? View.VISIBLE : View.GONE;
        switch (progressBarStyle) {
            case PROGRESS_BAR_STYLE_CIRCULAR:
                circularProgressBar.setVisibility(status);
                break;
            case PROGRESS_BAR_STYLE_NONE:
                if (customProgressBar != null) customProgressBar.setVisibility(status);
                break;
            case PROGRESS_BAR_STYLE_LINEAR:
            default:
                linearProgressBar.setVisibility(status);
        }
    }

    /**
     * If you want to customize the behavior of the webViewClient,
     * e.g. Override urls other than default telephone and email,
     * Use this method on WebViewSuite to submit the callbacks.
     * These callbacks will be executed after the codes in WebViewSuite are done.
     */
    public void customizeClient (WebViewSuiteCallback callback) {
        this.callback = callback;
    }

    /**
     * If you want to customize the settings of the webViewClient,
     * You cannot do it directly in onCreate() of your activity by getting WebView from WebViewSuite.
     * Why? Because the main point of this library is to delay the inflation - WebView is null in onCreate()!
     *
     * Therefore, I provided a callback for you to submit your own settings.
     * Use this method on WebViewSuite (This time in onCreate()) and submit the callback.
     * This callback will be executed after the default settings in WebViewSuite are completed.
     * I can assure you that webView is not null during interfereWebViewSetup().
     */
    public void interfereWebViewSetup (WebViewSetupInterference interference) {
        this.interference = interference;
    }

    public void setOpenPDFCallback (WebViewOpenPDFCallback callback) {
        this.openPDFCallback = callback;
    }

    public WebView getWebView () {
        return this.webView;
    }

    public ProgressBar getProgressBar (int progressBarStyle) {
        return progressBarStyle == PROGRESS_BAR_STYLE_LINEAR ? linearProgressBar : circularProgressBar;
    }

    private void setupWebView ()
    {
        webView.addJavascriptInterface(new WebAppInterface(context),"Android");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.i("hi","pageStarted");
                toggleProgressbar(true);
                if (callback != null) callback.onPageStarted(view, url, favicon);
                if(!isHide) {
                    webView.setVisibility(View.GONE);
                    logoImg.setVisibility(View.VISIBLE);
                    shrinkAnim.start();//animation start
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i("hi","pageFinished");
                toggleProgressbar(false);
                if (callback != null) callback.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                logoImg.setVisibility(View.GONE);
                if(shrinkAnim!=null)
                    shrinkAnim.cancel();//animation cancel.
                webView.bringToFront();
                isHide=false;
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:") && overrideTelLink) {
                    try {
                        Intent telIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        context.startActivity(telIntent);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } else if (url.startsWith("mailto:") && overrideEmailLink) {
                    try {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                        emailIntent.setData(Uri.parse("mailto:")); // only email apps should handle this
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{url.substring(7)});
                        if (emailIntent.resolveActivity(context.getPackageManager()) != null) {
                            context.startActivity(emailIntent);
                        }
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } else if (url.endsWith("pdf") && overridePdfLink)
                {
                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setType("application/pdf");
                    PackageManager packageManager=context.getPackageManager();
                    List list=packageManager.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY);
                    Intent pdfIntent = new Intent();
                    pdfIntent.setAction(Intent.ACTION_VIEW);
                    Uri uri = Uri.parse(url);
                    pdfIntent.setDataAndType(uri, "application/pdf");
                    pdfIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        context.startActivity(pdfIntent);
                    } catch (Exception e) {
                        Toast.makeText(context, "No Application Available to View PDF,Opening in browser", Toast.LENGTH_LONG).show();
                        String gUrl= "https://docs.google.com/gview?embedded=true&url="+url;
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(gUrl));
                        context.startActivity(browserIntent);
                    }
                    if (openPDFCallback != null) openPDFCallback.onOpenPDF();
                    return true;
                }
                else if(url.endsWith("jpg"))
                {
                    Log.i("hi","JPG URL TRIGGERED==");
                    return true;
                }
                else {
                    if (callback != null) {
                        return callback.shouldOverrideUrlLoading(webView, url);
                    } else {
                        return super.shouldOverrideUrlLoading(view, url);
                    }
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, FileChooserParams fileChooserParams)
            {
                // Double check that we don't have any existing callbacks
                if (mUploadMessage != null) {
                    mUploadMessage.onReceiveValue(null);
                }

                mUploadMessage = filePath;

                String fileType=null;
                if (fileChooserParams != null && fileChooserParams.getAcceptTypes() != null && fileChooserParams.getAcceptTypes().length > 0)
                {
                    //contentSelectionIntent.setType("image/*");
                    fileType=fileChooserParams.getAcceptTypes()[0];
                    fileType=fileType.equals("") ? "":fileType.substring(0,fileType.indexOf('/'));
                } else {
                    fileType="";
                }
                switch (fileType)
                {
                    case "image":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            if (context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                            {
                                launchCamera();
                            }
                            else
                            {
                                checkExternalStoragePermission(1);
                            }

                        }
                        else
                        {
                            launchCamera();
                        }
                        break;
                    case "audio":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                                launchRecordAudio();
                            }
                            else
                            {
                                checkExternalStoragePermission(2);
                            }
                        }
                        else
                        {
                            launchRecordAudio();
                        }
                        break;
                    case "video":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            if (context.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                            {
                                launchGallery();
                            }
                            else
                            {
                                checkExternalStoragePermission(3);
                            }
                        }
                        else
                        {
                            launchGallery();
                        }
                        break;
                    case "doc":
                    default:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            if (context.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                            {
                                launchFileChooser();
                            }
                            else
                            {
                                checkExternalStoragePermission(4);
                            }
                        }
                        else
                        {
                            launchFileChooser();
                        }
                        break;
                }
                return true;
            }
        });

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(enableJavaScript);
        webSettings.setBuiltInZoomControls(showZoomControl);
        webSettings.setSupportZoom(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setAppCacheEnabled(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setUseWideViewPort(true);

        /**
         * Download if there is an option in the Webpage.
         */
        webView.setDownloadListener(new DownloadListener()
        {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request( Uri.parse(url));
                request.setMimeType(mimetype);
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie",cookies);
                request.addRequestHeader("User-Agent",userAgent);
                request.setDescription("Downloading file...");
                request.setTitle(URLUtil.guessFileName(url,contentDisposition,mimetype));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url,contentDisposition,mimetype));
                DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        });
        webView.setVerticalScrollBarEnabled(enableVerticalScrollBar);
        webView.setHorizontalScrollBarEnabled(enableHorizontalScrollBar);

        if (interference != null) interference.interfereWebViewSetup(webView);
    }

    public void donotLoadDefaultPage() {
        dontLoadPage=true;
    }

    public interface WebViewSuiteCallback {
        void onPageStarted(WebView view, String url, Bitmap favicon);
        void onPageFinished(WebView view, String url);
        boolean shouldOverrideUrlLoading(WebView view, String url);
    }

    public interface WebViewSetupInterference {
        void interfereWebViewSetup(WebView webView);
    }

    public interface WebViewOpenPDFCallback {
        void onOpenPDF();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        File imageStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES)
                , AppConstants.APP_NAME);

        if (!imageStorageDir.exists()) {
            // Create AndroidExampleFolder at sdcard
            imageStorageDir.mkdirs();
        }

        // Create camera captured image file path and name
        File photoFile = new File(
                imageStorageDir + File.separator + "IMG_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg");
        return photoFile;
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt)
    {
        super.onScrollChanged(l, t, oldl, oldt);
        if(mOnScrollChangedCallback != null) mOnScrollChangedCallback.onScroll(l, t);
    }

    public OnScrollChangedCallback getOnScrollChangedCallback()
    {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback)
    {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback
    {
        public void onScroll(int horizontal, int vertical);
    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        public WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(final String toast) {
            Log.i("hi","Hello Toast called");
            mWebInterface.showToast(toast);

        }
        @JavascriptInterface
        public void set_status_bar_color(final String color,final String theme) {

            Log.i("hi","Hello called");
            mWebInterface.setStatusBarColor(color,theme);
        }
        @JavascriptInterface
        public void reload_page(final String msg) {
            // get_token(0);
            mWebInterface.reloadPage();
        }
    }



    /**
     * Function which seeks  External Storage permission from the user,and if permission is already present we will navigate to the Main screen.
     */
    private void checkExternalStoragePermission(int type)
    {
        String externalStorageCamera[]={Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
        String audio[] ={Manifest.permission.RECORD_AUDIO} ;
        String externalStorage[]={Manifest.permission.WRITE_EXTERNAL_STORAGE};
        String permissionGroup[]= null;

        if(type==1) {
            permissionGroup = externalStorageCamera;
        }else if(type==2) {
            permissionGroup = audio;
        }else if(type==3 || type==4){
            permissionGroup=externalStorage;
        }

        PermissionUtils.getInstance().grantPermission((MainActivity)context, permissionGroup, new PermissionUtils.Callback() {
            @Override
            public void onFinish(HashMap<String, Integer> permissionsStatusMap)
            {
                try
                {
                    if(permissionsStatusMap==null)
                    {
                        return;
                    }

                    int storageStatus=0;
                    if(type==1)
                    {
                        storageStatus = permissionsStatusMap.get(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        if(storageStatus == PermissionUtils.PERMISSION_GRANTED) {
                            storageStatus = permissionsStatusMap.get(Manifest.permission.CAMERA);
                        }

                    }
                    else if(type==2)
                    {
                        storageStatus = permissionsStatusMap.get(Manifest.permission.RECORD_AUDIO);
                    }
                    else
                    {
                        storageStatus = permissionsStatusMap.get(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }


                    switch (storageStatus)
                    {
                        case PermissionUtils.PERMISSION_GRANTED:
                            if(type==1) {
                                launchCamera();
                            }else if(type==2)
                            {
                                launchRecordAudio();
                            }else if(type==3)
                            {
                                launchGallery();
                            }
                            else if(type==4)
                            {
                                launchFileChooser();
                            }

                            break;
                        case PermissionUtils.PERMISSION_DENIED:
                            showRetryDialog(context.getString(R.string.denying_permission),type);
                            //Toast.makeText(ViosEnvironmentActivity.this,"PERMISSION DENIED",Toast.LENGTH_LONG).show();
                            break;
                        case PermissionUtils.PERMISSION_DENIED_FOREVER:
                            showRetryDialog(context.getString(R.string.deny_forever),type);
                            //Toast.makeText(ViosEnvironmentActivity.this,"PERMISSION DENIED FOREVER",Toast.LENGTH_LONG).show();
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                }
            }

        });

    }
    /**
     * Function to show a Retry dialog,This permission is required for downloading the pdfs and other media content.
     * @param msg
     * @param type
     */
    private void showRetryDialog(String msg, final int type)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(
                "allow",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        if(type==1)
                        {
                            checkExternalStoragePermission(1);
                        }
                        else if(type==2)
                        {
                            checkExternalStoragePermission(2);
                        }
                        else if(type==3)
                        {
                            checkExternalStoragePermission(3);
                        }
                        else
                        {
                            PermissionUtils.launchAppSettings(context);
                        }
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void launchCamera()
    {
        imagesList = new ArrayList<String>();
        Options options = Options.init()
                .setCount(5) //Number of images to restict selection count
                .setFrontfacing(false) //Front Facing camera on start
                //Pre selected Image Urls
                .setRequestCode(INPUT_CAMERA_REQUEST_CODE)//Request code for activity results
                .setPreSelectedUrls(imagesList)
                .setExcludeVideos(false) //Option to exclude videos
                .setVideoDurationLimitinSeconds(30) //Duration for video recording
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
                .setPath("/"+AppConstants.APP_NAME); //Custom Path For media Storage
        Pix.start((MainActivity)context, options);
    }

    private void launchRecordAudio()
    {
        Intent audio = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        ((MainActivity)context).startActivityForResult(Intent.createChooser(audio, "File Chooser"),
                INPUT_AUDIO_FILE_REQUEST_CODE);
    }

    private void launchGallery()
    {
        Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        gallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        ((MainActivity)context).startActivityForResult(gallery,INPUT_FILE_REQUEST_CODE);

    }

    private void launchFileChooser()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        ((MainActivity)context).startActivityForResult(Intent.createChooser(intent, "File Chooser"),
                INPUT_FILE_REQUEST_CODE);
    }
}
