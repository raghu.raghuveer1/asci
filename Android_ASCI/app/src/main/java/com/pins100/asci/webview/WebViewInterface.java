package com.pins100.asci.webview;

public interface WebViewInterface
{
    void setStatusBarColor(String color,String theme);
    void reloadPage();
    void showToast(String msg);
}
