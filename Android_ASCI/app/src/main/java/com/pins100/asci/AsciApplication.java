package com.pins100.asci;

import android.app.Application;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.pins100.asci.util.AppConstants;

import net.khirr.library.foreground.Foreground;

import org.json.JSONException;
import org.json.JSONObject;

public class AsciApplication extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();
        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        // OneSignal Initialization
        OneSignal.initWithContext(this);

        OneSignal.setAppId(AppConstants.ONESIGNAL_APP_ID);
        Foreground.Companion.init(this);

        /**
         * If required on use case basis we can enable this flags.
         */
        //OneSignal.unsubscribeWhenNotificationsAreDisabled(true);
        //OneSignal.pauseInAppMessages(true);
        //OneSignal.setLocationShared(false);
    }
}
