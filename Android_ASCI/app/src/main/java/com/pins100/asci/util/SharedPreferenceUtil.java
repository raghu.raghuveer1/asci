package com.pins100.asci.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.w3c.dom.Text;


public class SharedPreferenceUtil
{

    private static final String APP_SHARED_PREFS = "com.pins100.asci";

    private static final String REFERRAL_SHARED_PREF = "REFERRAL_SHARED_PREF";
    private static final String TOKEN_SHARED_PREF = "no_token";

    private static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public static boolean exists(Context context, String key) {
        return getSharedPreference(context).contains(key);
    }


    public static String getSavedReferralData(Context context) {
        return getSharedPreference(context).getString(REFERRAL_SHARED_PREF, "");
    }


    public static boolean getSavedTokenStatus(Context context) {
        return getSharedPreference(context).getBoolean(TOKEN_SHARED_PREF, false);
    }


    public static void saveUserReferralData(Context context, String referralData) {
        getSharedPreference(context).edit().putString(REFERRAL_SHARED_PREF, referralData).apply();
    }


    public static void saveTokenStatus(Context context, boolean tokenStatus) {
        getSharedPreference(context).edit().putBoolean(TOKEN_SHARED_PREF, tokenStatus).apply();
    }

}