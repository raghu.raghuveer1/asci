package com.pins100.asci.onesignal;

import android.content.Context;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationForeGroundHandler implements OneSignal.OSNotificationWillShowInForegroundHandler
{
    private NotificationForeGroundHandler mNotificationHandler;
    private Context mContext;
    public NotificationForeGroundHandler(NotificationForeGroundHandler notificationHandler,Context context)
    {
        mNotificationHandler=notificationHandler;
        mContext=context;
    }
    @Override
    public void notificationWillShowInForeground(OSNotificationReceivedEvent notificationReceivedEvent)
    {
        OSNotification notification = notificationReceivedEvent.getNotification();
        JSONObject additionalData = notification.getAdditionalData();
        try {
            int msgId = additionalData.getInt("sent_id");
            //Depending on usecase we can use this

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        notificationReceivedEvent.complete(null);
    }
}
