package com.pins100.asci.onesignal;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.onesignal.OSNotificationOpenedResult;
import com.onesignal.OneSignal;
import com.pins100.asci.util.AppConstants;
import com.pins100.asci.util.AppUtils;
import com.pins100.asci.util.NotificationHandler;
import com.pins100.asci.views.MainActivity;

import net.khirr.library.foreground.Foreground;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static com.pins100.asci.util.AppConstants.DISMISS;
import static com.pins100.asci.util.AppConstants.MARK_AS_READ_TYPE;
import static com.pins100.asci.util.AppConstants.QUIZ;

/**
 * Created by androidbash on 12/14/2016.
 */

public class NotificationOpenHandler implements OneSignal.OSNotificationOpenedHandler
{
    // This fires when a notification is opened by tapping on it.
    private NotificationHandler mNotificationHandler;
    private Context mContext;
    public NotificationOpenHandler(NotificationHandler notificationHandler,Context context)
    {
        mNotificationHandler=notificationHandler;
        mContext=context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenedResult result)
    {
        try
        {
            String actionId = result.getAction().getActionId();
            JSONObject notification = result.toJSONObject().getJSONObject("notification");
            JSONObject additionalData = notification.getJSONObject("additionalData");
            JSONArray actionButtons = notification.getJSONArray("actionButtons");

            if(actionId!=null&&!actionId.isEmpty())
            {
                String actionData = actionButtons.getString(Integer.parseInt(actionId)-1);
                JSONObject actionObject = new JSONObject(actionData);
                String typeText = actionObject.getString("text").toLowerCase();
                switch (typeText)
                {
                    case MARK_AS_READ_TYPE: //just mark the notification as read
                        int messageId = additionalData.getInt("received_message_id");
                        Log.i("","message id is "+messageId);
                        handleNow(mContext, messageId, false);
                        break;
                    case DISMISS:
                        break;
                    case QUIZ:
                        break;
                    default: //open the respective messages page
                        int msgId = additionalData.getInt("sent_id");
                        if(isAppIsInBackground(mContext))
                        {
                            Intent intent = new Intent(mContext, MainActivity.class);
                            intent.putExtra("msgId", msgId);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            mContext.startActivity(intent);
                        }
                        else {
                            mNotificationHandler.onNotificationOpened(AppUtils.getInstance().getNotificationToken(msgId, mContext));
                        }
                        break;
                }
            }
            else if(actionId==null)
            {
                int msgId = additionalData.getInt("sent_id");
                if(Foreground.Companion.isBackground())
                {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.putExtra("msgId", msgId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else {
                    mNotificationHandler.onNotificationOpened(AppUtils.getInstance().getNotificationToken(msgId, mContext));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleNow(Context context, int messageId, boolean notification)
    {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = AppConstants.BASE_URL_FOR_NOTIFICATIONS+"read/update/"+messageId+"/app";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("TAG","***************"+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("TAG","&&&&&&&&&&&&&&&&&"+error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
        }

        return isInBackground;
    }
}