package com.pins100.asci.views;

import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.fxn.pix.Pix;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;
import com.pins100.asci.BuildConfig;
import com.pins100.asci.R;
import com.pins100.asci.onesignal.NotificationOpenHandler;
import com.pins100.asci.util.AppUtils;
import com.pins100.asci.util.Log;
import com.pins100.asci.util.Log.Module;
import com.pins100.asci.util.NotificationHandler;
import com.pins100.asci.util.SharedPreferenceUtil;
import com.pins100.asci.webview.WebViewInterface;
import com.pins100.asci.webview.WebViewUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;

import eu.dkaratzas.android.inapp.update.Constants;
import eu.dkaratzas.android.inapp.update.InAppUpdateManager;
import eu.dkaratzas.android.inapp.update.InAppUpdateStatus;

public class MainActivity extends AppCompatActivity implements InAppUpdateManager.InAppUpdateHandler, NotificationHandler, OSSubscriptionObserver, WebViewInterface
{
    private WebViewUtil webViewSuite;
    private static final int REQ_CODE_VERSION_UPDATE = 100;
    private InAppUpdateManager inAppUpdateManager;
    private ImageView mLogoImg;
    private InstallReferrerClient mReferrerClient;
    private  ScaleAnimation shrinkAnim;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.white));
        }

        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        OneSignal.setNotificationOpenedHandler(new NotificationOpenHandler(this,MainActivity.this));
        OneSignal.addSubscriptionObserver(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        /**
         * action will be triggered if there is any app update avaialble in the Playstore.
         */
        inAppUpdateManager = InAppUpdateManager.Builder(this, REQ_CODE_VERSION_UPDATE)
                .resumeUpdates(true) // Resume the update, if the update was stalled. Default is true
                .mode(Constants.UpdateMode.FLEXIBLE)
                .snackBarMessage("An update has just been downloaded.")
                .snackBarAction("RESTART")
                .handler(this);

        inAppUpdateManager.checkForAppUpdate();

        mLogoImg=findViewById(R.id.ivLogo);

        /**Animation can be customized here : Duration can be increased decreased presently it is 500ms
         * Scale animation works as below fromX: 1.0f represents full image size toX: is to what size you want to shrink the image.
         * we are scaling 90% of the image.
         */
        shrinkAnim= new ScaleAnimation(1.0f, 0.10f, 1.0f, 0.10f, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
        shrinkAnim.setDuration(500); //500 milliseconds
        shrinkAnim.setFillAfter(true); //this helps in showing the shrinked image,else white screen will be displayed.
        mLogoImg.setAnimation(shrinkAnim);//Animation set

        webViewSuite = findViewById(R.id.webViewUtil);

        webViewSuite.setLoadImage(mLogoImg,shrinkAnim);

        webViewSuite.setWebInterface(this);

        webViewSuite.setOpenPDFCallback(new WebViewUtil.WebViewOpenPDFCallback() {
            @Override
            public void onOpenPDF()
            {

            }
        });

        if(getIntent().getExtras()!=null) {
            int msgId = getIntent().getIntExtra("msgId",0);
            if(msgId!=0)
            {
                webViewSuite.donotLoadDefaultPage();
                webViewSuite.startLoading(AppUtils.getInstance().getNotificationToken(msgId, MainActivity.this));
            }
        }

        /**
         *
         */
        mReferrerClient = InstallReferrerClient.newBuilder(this).build();
        mReferrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Connection established.
                        try{
                            ReferrerDetails response = mReferrerClient.getInstallReferrer();
                            String referrerUrl = response.getInstallReferrer();
                            Uri uri = Uri.parse("https://api.100pins.com?"+referrerUrl);
                            String server = uri.getAuthority();
                            String path = uri.getPath();
                            String protocol = uri.getScheme();
                            Set<String> args = uri.getQueryParameterNames();
                            try {
                                String source = uri.getQueryParameter("utm_source");
                                String referrer = uri.getQueryParameter("referrer");
                                Log.i(Module.UI,referrerUrl);
                                Log.i(Module.UI,source);
                                Log.i(Module.UI,referrer);
                                SharedPreferenceUtil.saveUserReferralData(MainActivity.this,referrer);
                            }catch (Exception exception){
                                SharedPreferenceUtil.saveUserReferralData(MainActivity.this,referrerUrl.replace('=','.'));
                            }
                            long referrerClickTime = response.getReferrerClickTimestampSeconds();
                            long appInstallTime = response.getInstallBeginTimestampSeconds();
                            boolean instantExperienceLaunched = response.getGooglePlayInstantParam();
                        }
                        catch (Exception exception){
                            Log.i(Module.UI,exception.toString());
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app.
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection couldn't be established.
                        break;
                }
            }
            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed()
    {
        if (!webViewSuite.goBackIfPossible())
            super.onBackPressed();
    }

    @Override
    public void onInAppUpdateError(int code, Throwable error) {

    }

    @Override
    public void onInAppUpdateStatus(InAppUpdateStatus status) {
        /*
         * If the update downloaded, ask user confirmation and complete the update
         */
        if (status.isDownloaded())
        {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.app_name))
                    .setMessage("An update has just been downloaded.")
                    .setPositiveButton("Complete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Triggers the completion of the update of the app for the flexible flow.
                            inAppUpdateManager.completeUpdate();
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQ_CODE_VERSION_UPDATE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                // If the update is cancelled by the user,
                // you can request to start the update again.
                inAppUpdateManager.checkForAppUpdate();
                Log.d(Module.UI, "Update flow failed! Result code: " + resultCode);
                return;
            }
        }
        /**Condition executed while we dont know which format to look**/
        else if (requestCode == webViewSuite.INPUT_FILE_REQUEST_CODE) {
            Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();
            if (result != null) {
                handleOnReceivedValue(data);
            } else {
                webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
            }
            webViewSuite.mUploadMessage = null;
        }
        /**
         * condition for Camera type request
         */
        else if (requestCode == webViewSuite.INPUT_CAMERA_REQUEST_CODE) {
            if(data==null)
            {
                webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
            }
            else {
                Uri[] results;
                ArrayList<String> imgsList = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                if(imgsList!=null&&imgsList.size()>0) {
                    try {
                        results=new Uri[imgsList.size()];
                        for (int i = 0; i < imgsList.size(); i++) {
                            File file = new File(imgsList.get(i).toString());
                            Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                            //imageAbsolutePath = imgsList.get(0);
                            results[i] = uri;
                        }
                        if (results != null) {
                            webViewSuite.mUploadMessage.onReceiveValue(results);
                        } else {
                            webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
                    }
                }
                else
                {
                    webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
                }
            }
            webViewSuite.mUploadMessage = null;

        }
        else if (requestCode == webViewSuite.INPUT_AUDIO_FILE_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK) {
                Uri audioUri = data.getData();
                if (audioUri != null) {
                    webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{audioUri});
                } else {
                    webViewSuite.mUploadMessage.onReceiveValue(new Uri[]{});
                }
                webViewSuite.mUploadMessage = null;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleOnReceivedValue(Intent data) {
        Uri[] results = null;
        if (data == null) {
        } else {
            String dataString = data.getDataString();
            ClipData clipData = data.getClipData();
            if (clipData != null) {
                results = new Uri[clipData.getItemCount()];
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);
                    results[i] = item.getUri();
                }
            }
            if (dataString != null)
                results = new Uri[]{Uri.parse(dataString)};
        }
        webViewSuite.mUploadMessage.onReceiveValue(results);
        webViewSuite.mUploadMessage = null;
    }

    @Override
    public void onNotificationOpened(String responseUri)
    {
        if(responseUri.equalsIgnoreCase(""))
        {
            //Load no internet
            webViewSuite.startLoading("file:///android_asset/no-internet.html");
        }else {
            webViewSuite.startLoading(responseUri);
        }
    }

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges)
    {
        if (!stateChanges.getFrom().isSubscribed() &&
                stateChanges.getTo().isSubscribed()) {
            Log.i(Module.NOTIFICATION, "subscription changed");
            // get player ID
            stateChanges.getTo().getUserId();
        }
        String result=AppUtils.getInstance().getToken(0,MainActivity.this);
        webViewSuite.startLoading(result);
        Log.i(Module.NOTIFICATION, "onOSPermissionChanged: " + result);
    }


    @Override
    public void setStatusBarColor(String color, String theme) {
        try {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    changeStatusBarWithTheme(color,theme);
                }
            });
        }catch (Exception exception){
            Log.i(Module.UI,exception.getClass().toString());
        }
    }

    @Override
    public void reloadPage()
    {
        //reload page
        webViewSuite.startLoading(AppUtils.getInstance().getToken(0,MainActivity.this));
    }

    @Override
    public void showToast(String msg) {
        try {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    //Show a toast message based on the requirement
                }
            });

        }catch (Exception exception){
            Log.i(Module.UI,exception.getClass().toString());
        }
    }

    private void changeStatusBarWithTheme(String color,String theme) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor(color));
        Log.i(Module.UI,"color=="+color+" Theme=="+theme);
        if(theme.equals("light")) {
            int flags = getWindow().getDecorView().getSystemUiVisibility();
            flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            getWindow().getDecorView().setSystemUiVisibility(flags);
        }else{
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
    }

    private void changeStatusBarColor(String color){
        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        Log.i(Module.UI,"color=="+color);
        if(color.equals("red")) {
            int flags = getWindow().getDecorView().getSystemUiVisibility();
            flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            getWindow().getDecorView().setSystemUiVisibility(flags);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.pins100));
        }
        else{
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.white));
        }
    }
}