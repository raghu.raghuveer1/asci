/*
 * Copyright (C) Vios Medical, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 */
package com.pins100.asci.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author 100Pins
 */
public final class Log {

    private static final String PREFIX = AppConstants.APP_NAME;
    private static final boolean DEBUG_ENABLED = true;
    ;
    private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            TIMESTAMP_FORMAT, Locale.US);
    private static final char SEPARATOR_COLON = ':';
    private static final char SEPARATOR_HYPHEN = '-';

    private static final  Log INSTANCE = new Log();

    /**
     * @return the instance
     */
    public static Log getInstance() {
        return INSTANCE;
    }

    /**
     *
     */
    private Log() {

    }
    private static String prepareTag(Module module) {
        return String.format("%s%c%s", PREFIX, SEPARATOR_HYPHEN, module);
    }

    public static void e(Module module, String message) {
        if (!DEBUG_ENABLED) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(timestamp()).append(SEPARATOR_HYPHEN);
        appendFileInfo(sb);
        sb.append(SEPARATOR_COLON).append(message);
        android.util.Log.e(prepareTag(module), sb.toString());
    }

    public static void e(Module module, String message, Throwable ex) {
       /* if (!DEBUG_ENABLED) {
            return;
        }*/
        StringBuilder sb = new StringBuilder();
        sb.append(timestamp()).append(SEPARATOR_HYPHEN);
        appendFileInfo(sb);
        sb.append(SEPARATOR_COLON).append(message);
        android.util.Log.e(prepareTag(module), sb.toString(), ex);
    }

    public static void i(Module module, String message) {
        android.util.Log
                .i(prepareTag(module), String.format("%s%c%s", timestamp(),
                        SEPARATOR_HYPHEN, message));
    }

    public static void d(Module module, String message) {
        if (!DEBUG_ENABLED) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(timestamp()).append(SEPARATOR_HYPHEN);
        appendFileInfo(sb);
        sb.append(SEPARATOR_COLON).append(message);
        android.util.Log.d(prepareTag(module), sb.toString());
    }

    private static String timestamp() {
        return Log.getInstance().DATE_FORMAT.format(new Date());
    }

    private static void appendFileInfo(StringBuilder sb) {
        final StackTraceElement stackTraceElement = Thread.currentThread()
                .getStackTrace()[4];

        String format = String.format(Locale.US, "%s%c%d",
                stackTraceElement.getFileName(), SEPARATOR_COLON,
                stackTraceElement.getLineNumber());
        sb.append(format);

    }

    public enum Module {
        UI,
        SERVICE,
        NOTIFICATION
        ;

        public String toString() {
            return name().toLowerCase(Locale.US);
        }

    }
}
