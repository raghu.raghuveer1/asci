package com.pins100.asci.util;

public class AppConstants
{
    public static int SPLASH_TIMEOUT=1000;
    public static String APP_NAME="Asci";
    public static String URL="https://pages.100pins.com/client/mriet/home";
    public static final String ONESIGNAL_APP_ID = "b21aadec-7603-4df2-a758-ce8860770040";
    public static final String BASE_URL_FOR_NOTIFICATIONS="https://api.100pins.com/api/";

    public static final String MESSAGE_NOTIFICATIONS_URL="https://pages.100pins.com/100pins/messages/";

    public static final String MARK_AS_READ_TYPE="mark as read";
    public static final String DISMISS="dismiss";
    public static final String QUIZ="quiz";
}
