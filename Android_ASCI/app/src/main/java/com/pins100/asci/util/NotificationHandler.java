package com.pins100.asci.util;

public interface NotificationHandler
{
    void onNotificationOpened(String uri);
}
