package com.pins100.asci.onesignal;

import android.content.Context;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;

public class NotificationServiceExtension implements OneSignal.OSRemoteNotificationReceivedHandler
{
    private static final String TAG = "Onesignal Notification";

    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent notificationReceivedEvent)
    {
        OSNotification notification = notificationReceivedEvent.getNotification();
        // If complete isn't call within a time period of 25 seconds, OneSignal internal logic will show the original notification
        // If null is passed to complete
        notificationReceivedEvent.complete(notification);
    }
}
