package com.pins100.asci.views;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.pins100.asci.R;
import com.pins100.asci.util.AppConstants;

/**
 * Splash Screen Activity will be loaded once you launch the application and will be dismissed after 3 seconds.
 */
public class SplashScreenActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

       // setContentView(R.layout.splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(SplashScreenActivity.this, R.color.white));
        }

       // SystemClock.sleep(1000);
        /**
         * Checking for the Android Version and displaying the permission dialog as required.
         */
        Intent intent=new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
        SplashScreenActivity.this.overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        finish();
        //navigateToWebActivity();

    }

    /**
     * function to navigate to the WebActivity,after navigating this activity will be closed.
     */
    private void navigateToWebActivity()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //After 2 Seconds navigate to WebView activity

            }
        }, AppConstants.SPLASH_TIMEOUT);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
