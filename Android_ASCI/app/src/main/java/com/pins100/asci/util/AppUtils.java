package com.pins100.asci.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

import com.onesignal.OSDeviceState;
import com.onesignal.OneSignal;
import com.pins100.asci.BuildConfig;

public class AppUtils
{

    private static final AppUtils INSTANCE = new AppUtils();
    /**
     * Private constructor
     *
     */
    private AppUtils() {
        super();
        //0-string, 1-int, 2-boolean
    }

    /**
     * @return the instance
     */
    public static AppUtils getInstance() {
        return INSTANCE;
    }

    public String getToken(int messageID, Context context)
    {
        if(NetworkUtils.isNetworkAvailable(context))
        {
            OSDeviceState device;
            device = OneSignal.getDeviceState();
            // Get new Instance ID token
            assert device != null;

            String token = device.getPushToken();
            com.pins100.asci.util.Log.i(com.pins100.asci.util.Log.Module.NOTIFICATION,"token is "+token);

            String deviceName = android.os.Build.MODEL;
            String deviceMan = android.os.Build.MANUFACTURER;
            String appVersion = BuildConfig.VERSION_NAME;

            String osVersion = Build.VERSION.RELEASE;
            String referrer = SharedPreferenceUtil.getSavedReferralData(context);
            String uri = Uri.parse(AppConstants.URL)
                    .buildUpon()
                    .appendQueryParameter("device_token", token)
                    .appendQueryParameter("device_manufacturer",deviceMan)
                    .appendQueryParameter("device_model",deviceName)
                    .appendQueryParameter("device_os","2")
                    .appendQueryParameter("app_version",appVersion)
                    .appendQueryParameter("device_os_version",osVersion)
                    .appendQueryParameter("referrer",referrer)
                    .appendQueryParameter("source", "onesignal")
                    .appendQueryParameter("message_id", String.valueOf(messageID))
                    .build().toString();
            com.pins100.asci.util.Log.i(com.pins100.asci.util.Log.Module.NOTIFICATION,uri);
            SharedPreferenceUtil.saveTokenStatus(context,false);
            return uri;

        }else {
            return "";
        }

    }

    public String getNotificationToken(int messageID, Context context)
    {
        if(NetworkUtils.isNetworkAvailable(context))
        {
            OSDeviceState device;
            device = OneSignal.getDeviceState();
            // Get new Instance ID token
            assert device != null;
            String token = device.getPushToken();
            com.pins100.asci.util.Log.i(com.pins100.asci.util.Log.Module.NOTIFICATION,"token is "+token);

            String deviceName = android.os.Build.MODEL;
            String deviceMan = android.os.Build.MANUFACTURER;
            String appVersion = BuildConfig.VERSION_NAME;

            String osVersion = Build.VERSION.RELEASE;
            String referrer = SharedPreferenceUtil.getSavedReferralData(context);
            String url=AppConstants.MESSAGE_NOTIFICATIONS_URL+"message/"+messageID;

            com.pins100.asci.util.Log.i(com.pins100.asci.util.Log.Module.NOTIFICATION,url);
            return url;

        }else {
            return "";
        }

    }

    /**
     * Function can be used if user disable notifications
     * @param context
     */
    public void notificationPermissions(Context context) {
        Intent notificationPermissionIntent = new Intent();
        notificationPermissionIntent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Android 8 and above
            notificationPermissionIntent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Android 5-7
            notificationPermissionIntent.putExtra("app_package", context.getPackageName());
            notificationPermissionIntent.putExtra("app_uid", context.getApplicationInfo().uid);
        }
        context.startActivity(notificationPermissionIntent);
    }
}
